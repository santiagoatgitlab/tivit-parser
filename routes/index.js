const express = require('express');
const router = express.Router();
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
	host : 'localhost:9200',
	log : 'trace'
});
const tivitParse = require('../modules/tivit-parse.js');
var nextId = 1;

router.get('/parse', function(req, res, next) {

	index = 'tivitbynode';

	client.indices.exists({
		index : index
	},function(error,exists){
		if (error) throw error;
		if (exists){
			indexCount(index,function(){
				readFile(index);
			})
		}
		else{
			readFile(index);
		}
	});
	res.end();	

});

function indexCount(index,callback){
	client.count({
		index : index
	},function(error,response){
		if (error) throw error;
		nextId = response.count + 1;
		callback();
	});
}

function readFile(index){
	const lineReader = require('readline').createInterface({
		input : require('fs').createReadStream('tivitFile.edi')
	})

	lineReader.on('line',function(line){
		var id_field;
		var fields = {}; 
		var type = parseInt(line.substring(0,1));
		switch(type){
			case 0 : 
				fields = tivitParse.parseHeader(line);	
				id_field = fields.establecimiento;
			break;
			case 1 : 
				fields = tivitParse.parseDetalleDelResumenDeOperaciones(line);	
				id_field = fields.numeroRO;
			break;
			case 2 :
				fields = tivitParse.parseDetalleDelComprobanteDeVenta(line);	
				id_field = fields.numeroRO;
			break;
			default : 
				console.log('default')
		}
		// chequeo que el objeto fields no esté vacio e inserto en elasticsearch
		if (!(Object.keys(fields).length === 0 && fields.constructor === Object)){
			createDocument(index,nextId,fields);
			nextId++;
		}		
	});
}

function createDocument(index,id,fields){
	client.create({
		index : index,
		type: 'doc',
		id : id,
		body : fields
	},function(error, response){
		if (error) throw error;
	})
}

module.exports = router;
