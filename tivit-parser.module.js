exports.parseHeader = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'establecimiento' : line.substring(1,11),
		'fechaProcesamiento' : line.substring(11,19),
		'periodoInicial' : line.substring(19,27),
		'periodoFinal' : line.substring(27,35),
		'Siguiente' : line.substring(35,42),
		'empresaAdquiriente' : line.substring(42,47),
		'opcionExtraccion' : line.substring(47,49),
		'VAN' : line.substring(49,50),
		'Buzon' : line.substring(50,70),
		'opcionDisenio' : line.substring(70,73),
		'usoCielo' : line.substring(73,250)
	};
	return fields;	
}

exports.parseDetalleDelResumenDeOperaciones = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'establecimiento' : line.substring(1,11),
		'numeroRO' : line.substring(11,18),
		'parte' : line.substring(18,20),
		'relleno' : line.substring(20,21),
		'plan' : line.substring(21,23),
		'tipoTransaccion' : line.substring(23,25),
		'fechaPresentacion' : line.substring(25,31),
		'fechaPago' : line.substring(31,37),
		'fechaEnvioBanco' : line.substring(37,42),
		'senialValorBruto' : line.substring(43,44),
		'bruto' : line.substring(44,57),
		'signoComision' : line.substring(57,58),
		'cantidadComision' : line.substring(58,71),
		'valorSenialRechazado' : line.substring(71,72),
		'valorRechazado' : line.substring(72,85),
		'senialValorNeto' : line.substring(85,86),
		'net' : line.substring(86,99),
		'banco' : line.substring(99,103),
		'agencia' : line.substring(103,108),
		'cuentaCorriente' : line.substring(108,122),
		'estadoPago' : line.substring(122,124),
		'cantidadCvsAceptados' : line.substring(124,130),
		'codigoProducto' : line.substring(130,132),
		'cantidadCvsRechazados' : line.substring(132,138),
		'identificadorSegundaMano' : line.substring(138,139),
		'fechaCapturaTransacciones' : line.substring(139,145),
		'ajusteFuente' : line.substring(145,147),
		'valorComplementario' : line.substring(147,160),
		'identificadorAnticipacion' : line.substring(160,161),
		'numeroOperacionAnticipacion' : line.substring(161,170),
		'senialValorBrutoAnticipado' : line.substring(170,171),
		'valorBrutoTemprana' : line.substring(171,184),
		'bandera' : line.substring(184,187),
		'numeroUnicoRO' : line.substring(187,209),
		'tasaComision' : line.substring(209,213),
		'rate' : line.substring(213,218),
		'comisionGarantia' : line.substring(218,222),
		'mediosCaptura' : line.substring(222,224),
		'numeroTerminalLogico' : line.substring(224,232),
		'codigoProducto' : line.substring(232,235),
		'matrizPago' : line.substring(235,245),
		'usoCielo' : line.substring(245,250)
	};
	return fields;	
}

exports.parseDetalleDelComprobanteDeVenta = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'establecimiento' : line.substring(1,11),
		'numeroRO' : line.substring(11,18),
		'numeroTarjeta' : line.substring(18,37),
		'fechaVenta' : line.substring(37,45),
		'senialValor' : line.substring(45,46),
		'precioCompra' : line.substring(46,59),
		'parte' : line.substring(59,61),
		'totalParcelas' : line.substring(61,63),
		'motivoRechazo' : line.substring(63,66),
		'codigoAutorización' : line.substring(66,72),
		'TID' : line.substring(72,92),
		'NSU' : line.substring(92,98),
		'cantidadaComplementario' : line.substring(98,111),
		'Tarjeta' : line.substring(111,113),
		'valorVenta' : line.substring(113,126),
		'siguienteValorEntrega' : line.substring(126,139),
		'numeroFactura' : line.substring(139,148),
		'emisionPlacaFuera' : line.substring(148,152),
		'numeroLogicoTerminal' : line.substring(152,160),
		'identificadorTasaEmbarque' : line.substring(160,162),
		'referenciaCodigo' : line.substring(162,182),
		'tiempoTransaccion' : line.substring(182,188),
		'numeroUnicoTransaccion' : line.substring(188,217),
		'indicadorCieloPromo' : line.substring(217,218),
		'usoCielo' : line.substring(218,250)
	};
	return fields;	
}

exports.parseDetalleDeLaOperacionDeAnticipos = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'establecimiento' : line.substring(1,11),
		'numeroOperacion' : line.substring(11,20),
		'fechaCreditoOperacion' : line.substring(20,28),
		'senialValorBrutoVista' : line.substring(28,29),
		'valorBrutoVista' : line.substring(29,42),
		'senialValorBrutoParcelada' : line.substring(42,43),
		'valorBrutoParcelada' : line.substring(43,56),
		'senialValorBrutoElectrica' : line.substring(56,57),
		'valorBrutoTranvia' : line.substring(57,70),
		'senialValorBrutoTotal' : line.substring(70,71),
		'valorBrutoTotal' : line.substring(71,84),
		'senialValorNetoVista' : line.substring(84,85),
		'valorNetoVista' : line.substring(85,98),
		'senialValorNetoParcelada' : line.substring(98,99),
		'valorNetoParcelada' : line.substring(99,112),
		'senialValorNetoPredatado' : line.substring(112,113),
		'valorNetoPredatado' : line.substring(113,126),
		'senialValorNetoTotal' : line.substring(126,127),
		'valorNetoTotal' : line.substring(127,140),
		'tasaDescuentoAnticipation' : line.substring(140,145),
		'banco' : line.substring(145,149),
		'agencia' : line.substring(149,154),
		'cuentaCorriente' : line.substring(154,168),
		'senialValorNetoAnticipacion' : line.substring(168,169),
		'valorNetoAnticipacion' : line.substring(169,182),
		'usoCielo' : line.substring(182,250)
	};
	return fields;	
}

exports.parseDetalleDeLosROsAnticipados = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'establecimiento' : line.substring(1,11),
		'numeroOperacion' : line.substring(11,20),
		'fechaVencimientoRO' : line.substring(20,28),
		'numeroROAnticipado' : line.substring(28,35),
		'parcelaAnticipada' : line.substring(35,37),
		'totalParcelas' : line.substring(37,39),
		'senialValorBrutoOriginal' : line.substring(39,40),
		'valorBrutoOriginal' : line.substring(40,53),
		'senialValorNetoOriginal' : line.substring(53,54),
		'valorNetoOriginal' : line.substring(54,67),
		'senialValorBrutoAnticipacion' : line.substring(67,68),
		'valorBrutoAnticipacion' : line.substring(68,81),
		'senialValorNetoAnticipacion' : line.substring(81,82),
		'valorNetoAnticipacion' : line.substring(82,95),
		'bandera' : line.substring(95,98),
		'numeroUnicoRO' : line.substring(98,120),
		'usoCielo' : line.substring(120,250)
	};
	return fields;	
}

exports.parseDetalleDeLaDeudaDeLosROsAnticipados = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'establecimiento' : line.substring(1,11),
		'numeroROOriginal' : line.substring(11,33),
		'numeroROAnticipado' : line.substring(33,40),
		'fechaPagoROAnticipado' : line.substring(40,48),
		'senialValorROAnticipado' : line.substring(48,49),
		'valorROAnticipado' : line.substring(49,62),
		'numeroUnicoROVentaOrigenAjuste' : line.substring(62,84),
		'numeroROAjusteCargo' : line.substring(84,91),
		'fechaPagoAjuste' : line.substring(91,99),
		'senialValorAjusteDeuda' : line.substring(99,100),
		'valorAjusteCargo' : line.substring(100,113),
		'senialValorCompensado' : line.substring(113,114),
		'valorCompensado' : line.substring(114,127),
		'senialSaldoROAnticipado' : line.substring(127,128),
		'valorSaldoROAnticipado' : line.substring(128,141),
		'usoCielo' : line.substring(141,250)
	};
	return fields;	
}

exports.parseTrailer = function(line){
	fields = {
		'tipo' : line.substring(0,1),
		'totalRegistros' : line.substring(1,12),
		'usoCielo' : line.substring(12,250)
	};
	return fields;	
}
