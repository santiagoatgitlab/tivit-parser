const express = require('express');
const async	= require('async');
const lineByLineReader = require('line-by-line');
const router = express.Router();
const elasticsearch = require('elasticsearch');
const fs = require('fs');
const client = new elasticsearch.Client({
	host : 'localhost:9200',
	log : 'trace'
});
const tivitParse = require('tivit-parser');
var nextId = 1;

const index = 'many_files';
const inputDir = '/all/workspace/ficus/prinsen/tivit/in/';
const outputDir = '/all/workspace/ficus/prinsen/tivit/out/';

var lineReader;
var files;

client.indices.exists({
	index : index
},function(error,exists){
	if (error) throw error;
	if (exists){
		indexCount(index,function(){
			readFiles(index);
		})
	}
	else{
		readFiles(index);
	}
});

function indexCount(index,callback){
	client.count({
		index : index
	},function(error,response){
		if (error) throw error;
		nextId = response.count + 1;
		callback();
	});
}

function readFiles(){
	fs.readdir(inputDir, (err, fileNames) => {
		files = fileNames;
		if (files.length > 0){
			readFile(0);
		}
		else{
			console.log('no files to read');
		}

	});
}

function readFile(fileIndex){
	//levanto el archivo del direcotorio de entrado
	lineReader = new lineByLineReader(inputDir+files[fileIndex]);
	var lastLine = false;

	lineReader.on('error',function(err){
		throw err;
	});

	lineReader.on('line',function(line){
		lineReader.pause();
		var id_field;
		var fields = {}; 
		var type = parseInt(line.substring(0,1));
		switch(type){
			case 0 : 
				fields = tivitParse.parseHeader(line);	
				id_field = fields.establecimiento;
			break;
			case 1 : 
				fields = tivitParse.parseDetalleDelResumenDeOperaciones(line);	
				id_field = fields.numeroRO;
			break;
			case 2 :
				fields = tivitParse.parseDetalleDelComprobanteDeVenta(line);	
				id_field = fields.numeroRO;
			break;
			case 5 :
				fields = tivitParse.parseDetalleDeLaOperacionDeAnticipos(line);	
				id_field = fields.numeroRO;
			break;
			case 6 :
				fields = tivitParse.parseDetalleDeLosROsAnticipados(line);	
				id_field = fields.numeroRO;
			break;
			case 7 :
				fields = tivitParse.parseDetalleDeLaDeudaDeLosROsAnticipados(line);	
				id_field = fields.numeroRO;
			break;
			case 9 :
				fields = tivitParse.parseTrailer(line);	
				id_field = fields.numeroRO;
				lastLine = true;
			break;
			default : 
				console.log('default')
		}
		// chequeo que el objeto fields no esté vacio e inserto en elasticsearch
		if (!(Object.keys(fields).length === 0 && fields.constructor === Object)){
			createDocument(index,nextId,fields,fileIndex,lastLine);
			nextId++;
		}		
	});
}

function createDocument(index,id,fields,fileIndex,lastLine){
	client.create({
		index : index,
		type: 'doc',
		id : id,
		body : fields
	},function(error, response){
		if (error) throw error;
		lineReader.resume();
		//Muevo el archivo ya parseado al directorio de salida
		if(lastLine){
			fs.rename(inputDir+files[fileIndex],outputDir+files[fileIndex],function(error){
				if (error) throw error;
				if (files.length > fileIndex+1){
					readFile(fileIndex+1);
				}
				else{
					console.log( (fileIndex+1) + ' files were read');
				}
			});
		}
	})
}

module.exports = router;
